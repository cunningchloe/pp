from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template
import pimple_the_app 

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    #url(r'^$', direct_to_template, {'template':'index.html'}),  # just testing template
    url(r'^$','pimple_the_app.views.index_view'),
    url(r'^play_it/$', 'pimple_the_app.views.play_it'),
    url(r'^controls/$', 'pimple_the_app.views.control_playing_file'),
    url(r'^youtube_play_it/$', 'pimple_the_app.views.youtube_download_and_play'),
    url(r'^play_loopable/$','pimple_the_app.views.play_loopable'),
    # Examples:
    # url(r'^$', 'pp-the_project.views.home', name='home'),
    # url(r'^pp-the_project/', include('pp-the_project.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
