# below from notes.py, t.py



#############################################
#############################################
#                                           #
# VIEWS                                     #
#   conclusion, for now: way (2) below      # 
#   seems way easier. for now.              #
#                                           #
#############################################
#############################################

# def play_this(request, next=the_var_from_url_passed_in_like_this_maybe):
"""
#############################################
# views                                     #
#  assumes that the name of 
#   the file to play DOES come in the url
#   -- see url (1)  above
#############################################
"""
""" 
file_to_play_path =the_var_from_url_passed_in_like_this???
if file_to_play_path:    # i.e. there was something in request.POST['submit']
    # os.system("startx") if it's not already running
    return_val = omxplay(file_to_play_path, [paramater1, parameter2])
    # learn **kwargs and all that shit!!!
   
    ###########  
    #  so... would it do a return_val right away? like, everythign's ok, I'm running??
    # 
    # so then it would return HttpResponse for the "this is what's playing" page
    # or 
 # else:
    # I guess retun HttpResponse for the "there is no_file_to_play_path" page 
    """
import os
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from pprint import pprint
from django.template import Context, RequestContext
#from pyomxplayer import OMXPlayer   # the original kept erroring...  see my comments here ____________________ and comments in pyomxplayer_nk_fix.py
from pyomxplayer_nk_fix import pyomxplayer_nk_fix

MY_MEDIA_DIR = "/home/pi/play_media/"    # interim....

# the omxplayer object needs to be available to control_playing_file, but it's returned from the function
# 	that does the playing... and the controls function is a separate thing, and gets called separately in urls.py
# 	So making it a global variable right now...  BUT ... THIS IS PROBABLY YET MORE INDICATION THAT YOU NEED TO COMBINE ALL
# 	THE PLAY FUNCTIONS ... AND THE CONTROLS FUNCTION AS WELL!!!!!!!!
# 	so global blah blah 
# 	or is there another way.... send to anotehr function? ;
 
# maybe combine with play_it below
def play_loopable(request):
  """ like play_it, but for loopables """
  context = {}

  # need list of playable files and loopable playable files for index template
  playable_files = filter_playable_files(MY_MEDIA_DIR)  # return only playable files (for now just look for playable files ext)
  context["playable_files_list"] = playable_files
  playable_loopable_files = filter_playable_files(MY_MEDIA_DIR+"/loopables")  
  context["playable_loopable_files_list"] = playable_loopable_files

  # flag for whether there is a file currently playing. Template displays 
  #   the currently-playing div only if this is true
  is_file_playing = 0
  context["is_file_playing"] = is_file_playing 


  if "loopable_file_to_play" in request.POST:
	  from_youtube = False
	  file_to_play_path = MY_MEDIA_DIR + request.POST["loopable_file_to_play"] #full path
	  context["currently_playing_file_name"] = request.POST["loopable_file_to_play"]

  if "num_loops" in request.POST:
	  num_loops = request.POST["num_loops"]
	  context["num_loops"] = num_loops  # don't need it in context for anything other than the debug box. so far.

  # handle exceptions/watch out for stuff not being in request.POST above...
  omx = somehow_run_omxplayer(file_to_play_path, num_loops=num_loops)
  #context["omx_object"]=omx # will this work??????????????????????????????  I need this to be available to control_playing_files
  if 'current_omx_object'in request.session.keys():
	del request.session['current_omx_object']
  request.session['current_omx_object'] = omx
  #request.session['current_omx_object'] = "I am actually a fucking string"
  
  print "out of somehow_run_omxplayer now (loopables)", file_to_play_path
  context["is_file_playing"]=1

  #next = request.GET.get('next','')
  return render_to_response('index.html', context, context_instance=RequestContext(request))
  # no, fix, don't return eerything in the context thing.... right??

	   # for later....    because should just call play_it below
	   # return 'index.html', context, RequestContext(request)



    


# combine with above later... probly just a loopables flag
def play_it(request,path_to_youtube_vid=None):
    """  omxplay the file. Also get list of playable files to send to template next
        [????????]
    """
    context = {}

    # need list of playable files and loopable playable files for index template
    playable_files = filter_playable_files(MY_MEDIA_DIR)  # return only playable files (for now just look for playable files ext)
    context["playable_files_list"] = playable_files
    playable_loopable_files = filter_playable_files(MY_MEDIA_DIR+"/loopables")  
    context["playable_loopable_files_list"] = playable_loopable_files
    
    # flag for whether there is a file currently playing. Template displays 
    #   the currently-playing div only if this is true
    is_file_playing = 0
    context["is_file_playing"] = is_file_playing 

    from_youtube = 0

    if "file_to_play" in request.POST:
        from_youtube = False
        file_to_play_path = MY_MEDIA_DIR + request.POST["file_to_play"] #full path
        context["currently_playing_file_name"] = request.POST["file_to_play"]

    elif path_to_youtube_vid:  # if true coming in from youtube view 
        from_youtube = True 
        file_to_play_path = path_to_youtube_vid    # or actually just the last part? not checking the dir here? 
        context["currently_playing_file_name"] = os.path.basename(file_to_play_path)

    if file_to_play_path:
	  omx = somehow_run_omxplayer(file_to_play_path)
	  #context["omx_object"]=omx # will this work??????????????????????????????  I need this to be available to control_playing_files
	  if 'current_omx_object'in request.session.keys():
		del request.session['current_omx_object']
	  request.session['current_omx_object'] = omx
  	  #request.session['current_omx_object'] = "I am actually a fucking string"
	  print "out of somehow_run_omxplayer now ", file_to_play_path
	  print "but is omx object still alive?  i had to pass it through, right????? here: ", omx
	  print "--------------------------------------------------------"
	  context["is_file_playing"]=1

	  if not from_youtube:
		  #next = request.GET.get('next','') 
		  print "\n\n====== 1 ===================>  about to return, wtf is going on\n\n"
		  return render_to_response('index.html', context, context_instance=RequestContext(request))
		  # no, fix, don't return eerything in the context thing.... right??

	  else:  # from youtube, so return info to the youtube view so it can return a response
		  return 'index.html', context, RequestContext(request)



   # return HttpResponseRedirect(next, context=context) 
    
        # so then it would return HttpResponse for the "this is what's playing" page
        #       Above: important info on what else would be on the page that's returned
        # or 
     # else:
        # I guess retun HttpResponse for the "there is no_file_to_play_path" page 


def youtube_download_and_play(request):
    """ Download youtube using youtube-dl to media dir, then play it
    """

    # youtube vid downloaded to play_media, or MY_MEDIA_DIR, for now. 
   # vid_url = "http://www.youtube.com/watch?v=yO78qoyOQJc"  #testing for now so hardcoded
    # but should come in with form
    
    # ..... check if request.POST, etc. ...........
    vid_url = request.POST['youtube_link']

    vid_dl_command = 'cd '+MY_MEDIA_DIR+';'+'youtube-dl.py "'+vid_url+'"'  
    # change to the media dir and run youtube-dl in it. Maybe better way than cd, 
    #   just hacking it for now

    os.system(vid_dl_command)
    """ This is what it printed in interactive session: 
     os.system( youtube-dl.py http://www.youtube.com/watch?v=yO78qoyOQJc)
    [youtube] Setting language
    [youtube] yO78qoyOQJc: Downloading video webpage
    [youtube] yO78qoyOQJc: Downloading video info webpage
    [youtube] yO78qoyOQJc: Extracting video information
    [download] Destination: yO78qoyOQJc.mp4
    [download] 100.0% of 18.23M at    1.31M/s ETA 00:00
    0

    (last is actually return code)
    """
    youtube_vid_path = os.path.join(MY_MEDIA_DIR,os.path.basename(vid_url)[len('watch?v='):]+'.mp4')  # actually just taking basename here would give you watch?v=20384032849,for ex., not just the number, so ignore the "watch?v=" part
    # default youtube-dl behavior is to download mp4 into current dir, w name same
    #   as last part of url

    template_name, context_param, context_instance_param = play_it(request,youtube_vid_path)

    # now return response like just normal play/file selection button click 
    return render_to_response(template_name, context_param, context_instance_param)


def index_view(request):
    """ Generate information provided to the initial view, 
        e.g. list of files in media dir
    """
    context={}
    # need list of playable files and loopable playable files for index template
    playable_files = filter_playable_files(MY_MEDIA_DIR) # return only playable files (for now, just look for playable files ext)
    context["playable_files_list"] = playable_files
    playable_loopable_files = filter_playable_files(os.path.join(MY_MEDIA_DIR,"loopables"))  
    context["playable_loopable_files_list"] = playable_loopable_files
    return render_to_response('index.html', context, context_instance=RequestContext(request))



def filter_playable_files(path_to_dir):  # dir: play_media, play_media/loopables
    """ Take a list of file names and return only those that omxplayer can play. 
        (But for now, just return ones with certain ext)
    """
    file_names_list = os.listdir(path_to_dir)    
    ret_list = []
    for file_name in file_names_list:
        if file_name.endswith(('.avi','.mkv','.mp3','.mp4','m4v','flv')):  # omxplayer can handle these, as discovered through trial and error... not anything smarter like looking it up or looking at the source code!
            ret_list.append(file_name)
            
    return ret_list




def somehow_run_omxplayer(path_to_file, num_loops=1):
  """ This is temp... 
	  Create OMXPlayer object and start playing it the specified number of times (num_loops)
  """

   # not implementing loopage right now......
   # it's not as easy as if num_loops > 1: and looping here..........
   #        it has to be like..  if a file just stopped check if it was supposed to be looped. 
   #        also, controls should not interrupt that. i.e. can pause a loopable file, and it won't kill the loopage
  omx = pyomxplayer_nk_fix.OMXPlayer(path_to_file, start_playback=True)
  print "*"*20, "started omxplayer/playing THEORETICALLY", "*"*20
  print "*"*20, "omx, path_to_file: ", omx, path_to_file,"*"*20
  return omx




  """ don't delete this note yet!!!!!!!!!!!


  see more at the very end of this file about what to write (in a metamad post and/or comments) concerning all this launching of a separate process. 

  pyomxplayer:  note that if you installed it already you may have been using the non-pexpect version:
  so do again
  git clone https://github.com/jbaiter/pyomxplayer.git
  python pyomxplayer/setup.py install

  pyomxplayer: wrapper module for OMXPlayer. https://github.com/jbaiter/pyomxplayer 
	  ' Unlike other implementations, this module does not rely on any external scripts and FIFOs, but uses the pexpect module for communication with the OMXPlayer process.' 

  >>> from pyomxplayer import OMXPlayer
  >>> from pprint import pprint
  >>> omx = OMXPlayer('/tmp/video.mp4') #  so this just means play, actually
  >>> pprint(omx.__dict__)
  {'_position_thread': <Thread(Thread-5, started 1089234032)>,
  '_process': <pexpect.spawn object at 0x1a435d0>,
  'audio': {'bps': 16,
		  'channels': 2,
		  'decoder': 'mp3',
		  'rate': 48000,
		  'streams': 1},
  'chapters': 0,
  'current_audio_stream': 1,
  'current_volume': 0.0,
  'paused': True,
  'position': 0.0,
  'subtitles': 0,
  'subtitles_visible': False,
  'video': {'decoder': 'omx-mpeg4',
		  'dimensions': (640, 272),
		  'fps': 23.976025,
		  'profile': 15,
		  'streams': 1}}
  >>> omx.toggle_pause()
  >>> omx.position
  9.43
  >>> omx.stop()
    # can I create omx object without making it play right away? 
    # YES: 
    # omx = OMXPlayer('/path/to/vid/', start_playback=False)
    # then, to play: omx.toggle_pause() 
    # the only other options: toggle_pause, toggle_subtitles, stop
    #  the following not implemented yet. but maybe YOU should do it.  and then tell the author. 
    #          although actually the way his are done, with the extra argument, blah ... i dunno, i don't get, i'd rather have seek_left, right, not
    #          jjust seek but whatever. just add your own, leave his alone. 
	def set_speed(self):
		raise NotImplementedError

	def set_audiochannel(self, channel_idx):
		raise NotImplementedError

	def set_subtitles(self, sub_idx):
		raise NotImplementedError

	def set_chapter(self, chapter_idx):
		raise NotImplementedError

	def set_volume(self, volume):
		raise NotImplementedError

	def seek(self, minutes):
		raise NotImplementedError


	Here are all the controls: 
		Key bindings     Key Action
		z           Show Info
		1           Increase Speed
		2           Decrease Speed
		j           Previous Audio stream
		k           Next Audio stream
		i           Previous Chapter
		o           Next Chapter
		n           Previous Subtitle stream
		m           Next Subtitle stream
		s           Toggle subtitles
		q           Exit OMXPlayer
		Space or p  Pause/Resume
		-           Decrease Volume
		+           Increase Volume
		Left Arrow  Seek -30
		Right Arrow Seek +30
		Down Arrow  Seek -600
		Up Arrow    Seek +600

	So here's how other ones are implemented, so use that as a guide:
		_PAUSE_CMD = 'p'
		_TOGGLE_SUB_CMD = 's'
		_QUIT_CMD = 'q'

		 def toggle_pause(self):
			if self._process.send(self._PAUSE_CMD):
				self.paused = not self.paused
		def toggle_subtitles(self):
			if self._process.send(self._TOGGLE_SUB_CMD):
				self.subtitles_visible = not self.subtitles_visible
		def stop(self):
			self._process.send(self._QUIT_CMD)
			self._process.terminate(force=True)


	So would have to add e.g. 
	_NEXT_CH_CMD = 'o'
	def next_ch(self):
		self._process.send(self._NEXT_CH_CMD)
		# anything else?
		
	How to send arrows (i.e. Left Seek, Right Seek, etc. 
	OK, right now just downloaeded the actual zip of omxplayer.  literally just grep through it for "seek," etc. to see how the arrows are read. 
		OK, here is the pertinent code, from omxplayer.cpp.  see the case stamenents. 
		OK, this:
			  case 0x5b44: // key left
				if(m_omx_reader.CanSeek()) m_incr = -30.0;
				break;
			  case 0x5b43: // key right
				if(m_omx_reader.CanSeek()) m_incr = 30.0;
				break;
			  case 0x5b41: // key up
				if(m_omx_reader.CanSeek()) m_incr = 600.0;
				break;
			  case 0x5b42: // key down
			  So just play around with that.

  """

#######################
# note!!!!!!!!!!!!!!!!!!!     not doing the right checking with request.POST anywhere. fix. 
def control_playing_file(request):
  """ Pause/play/rewind/etc. the currently playing file. 
	  Currently playing file is (for now) just a hidden field in the form 
  """
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print "~~~~~~~~~~~~~   UH WTF ????????????~~ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  # need currently playing file's pyomxplayer object, which I've been referring to as omx......  I had it in context .....

  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print "session['current_omx_object': ", request.session.get('current_omx_object')
  print "\n\n =================== 2 ========================="
  print "request: ", request
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  context = {}    # am I reinventing the wheel by building context like this in all the views? or just missing something? ask arkadiy or look up contexts!!!!!!!!!!!
  # doing this because  need list of playable files and loopable playable files for index template


  #curr = request.POST['currently_playing_file']
  omx = request.session.get('current_omx_object')
  print "~~~~~~~~~~   omx ????????????????~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  print omx
  print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  # NOPE!  'unicode' object has no attribute 'toggle_pause'
  # because  u'omx_object_i_guess': [u'<pyomxplayer_nk_fix.pyomxplayer_nk_fix.OMXPlayer']}   ........
  # So...?   how to pass OBJECT in django views.....  https://docs.djangoproject.com/en/dev/topics/http/sessions/#session-object-guidelines?
  # sessions? 

	#################### hello!!!!!!!!!!!!!!! #######################
	##############   need omx object! ########################
  if 'pause' or 'play' in request.POST:
	  omx.toggle_pause()

	  # reset file playing status
	  if request.POST['is_file_playing']:
		  context["is_file_playing"]=1
	  else:
		  context["is_file_playing"]=0
	  print "***************************************************************"
	  print "pause or play", 
	  print "***************************************************************"
            
  elif 'stop' in request.POST:
	  omx.stop()
	  context["is_file_playing"]=0
	  print "***************************************************************"
	  print "stopped", 
	  print "***************************************************************"
    #  elif 'rew':  # and ff, etc.  
    #   omx.rewind()   # so would be same as back arrow. note not implemented. but maybe you should do it and tell the
    #   dude~!!!!!!1!!!!!!!!!!!!!!!!!!!
    #  do google search for omxplayer commmands (it's some rpi post, can't remember) .  
    #       also there should be next chapter, prev chapter. 
    

  """ ugh look how you keep repeating this building of the context thing..........    i am probly just duplicating something django can do.    ask arkadiy later. or fucking look up context shit. 
  """
  # doing this because  need list of playable files and loopable playable files for index template
  playable_files = filter_playable_files(MY_MEDIA_DIR)  # return only playable files (for now just look for playable files ext)
  context["playable_files_list"] = playable_files
  playable_loopable_files = filter_playable_files(MY_MEDIA_DIR+"/loopables")  
  context["playable_loopable_files_list"] = playable_loopable_files





  return render_to_response('index.html', context, context_instance=RequestContext(request))


"""

SO..............   metamad post !~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


but note, about the difficulties with doing os.system calls for interactive things. 

so pexpect was ultimately the answer........

make note about how this works in the code!!!!!    before githubbing. 
just quickly summarize how was trying to do os.system etc. but pexpect blah blah. 


---------- Forwarded message ----------
From: Nadia Karlinsky <karlinsk@illinois.edu>
Date: Sat, Dec 8, 2012 at 6:28 PM
Subject: subprocesses that wait for input - python
To: Steve Granda <sjgranda@gmail.com>


!!!!!!!!

http://pexpect.sourceforge.net/pexpect.html

For example::
 
      child = pexpect.spawn('scp foo myname@host.example.com:.')
               child.expect ('Password:')
                            child.sendline (mypassword)


.........  AND THESE WERE THE OLD ALTERNATIVES..........
Here's a summary of the ways to call external programs and the advantages and disadvantages of each:

(1) os.system("some_command with args") passes the command and arguments to your system's shell. 
- This is nice because you can actually run multiple commands at once in this manner and set up pipes and input/output redirection. 
- For example,
        os.system("some_command < input_file | another_command > output_file")
- However, while this is convenient, you have to manually handle the escaping of shell characters such as spaces, etc. On the other hand, this also lets you run commands which are simply shell commands and not actually external programs.
- http://docs.python.org/lib/os-process.html

(2) stream = os.popen("some_command with args") will do the same thing as os.system except that it gives you a file-like object that you can use to access standard input/output for that process. 
- There are 3 other variants of popen that all handle the i/o slightly differently. If you pass everything as a string, then your command is passed to the shell; if you pass them as a list then you don't need to worry about escaping anything.
- http://docs.python.org/lib/os-newstreams.html

(3) The Popen class of the subprocess module. 
_ This is intended as a replacement for os.popen but has the downside of being slightly more complicated by virtue of being so comprehensive. 
- For example, you'd say
        print Popen("echo Hello World", stdout=PIPE, shell=True).stdout.read()
    instead of
        print os.popen("echo Hello World").read()
    but it is nice to have all of the options there in one unified class instead of 4 different popen functions.
- http://docs.python.org/lib/node528.html

(4) The call function from the subprocess module. 
- This is basically just like the Popen class and takes all of the same arguments, but it simply wait until the command completes and gives you the return code. 
- For example:
        return_code = call("echo Hello World", shell=True)
- http://docs.python.org/lib/node529.html

The os module also has all of the fork/exec/spawn functions that you'd have in a C program, but I don't recommend using them directly.

The subprocess module should probably be what you use.
"""


