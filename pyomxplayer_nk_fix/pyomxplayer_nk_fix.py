import pexpect
import re

from threading import Thread
from time import sleep


"""  (nk)
My changes in here from the original at https://github.com/jbaiter/pyomxplayer as of 12/23/12:  
- added the comment about it breaking and the extra aspect_props readline
- changed start_playback to default to True, but I may have already accounted for that in my views
  code...

To do:
- smarter parsing for video, audio, file (and maybe aspect) properties
- extra commands for additional controls like volume, fast-forwarding, etc. 
	(I have some 'draft' code for this, perhaps in comments in views.py or something?)

note.... posted on this here: http://www.raspberrypi.org/phpBB3/viewtopic.php?f=38&t=10599


ARGGGGhHHHH fucking update: 
- well, back to commenting out shit and hardcoding shit. See the threading or whatever error below
"""

""" nk

    Here are all the controls: 
        Key bindings     Key Action
        z           Show Info
        1           Increase Speed
        2           Decrease Speed
        j           Previous Audio stream
        k           Next Audio stream
        i           Previous Chapter
        o           Next Chapter
        n           Previous Subtitle stream
        m           Next Subtitle stream
        s           Toggle subtitles
        q           Exit OMXPlayer
        Space or p  Pause/Resume
        -           Decrease Volume
		+           Increase Volume
        Left Arrow  Seek -30 
        Right Arrow Seek +30 
        Down Arrow  Seek -600
        Up Arrow    Seek +600

    How to send arrows (i.e. Left Seek, Right Seek, etc. 
    OK, right now just downloaeded the actual zip of omxplayer.  literally just grep through it for "seek," etc. to see how the arrows are read. 
        OK, here is the pertinent code, from omxplayer.cpp.  see the case stamenents. 
			  case 'q':
				m_stop = true;
				goto do_exit;
				break;
              case 0x5b44: // key left
                if(m_omx_reader.CanSeek()) m_incr = -30.0;
                break;
              case 0x5b43: // key right
                if(m_omx_reader.CanSeek()) m_incr = 30.0;
                break;
              case 0x5b41: // key up
                if(m_omx_reader.CanSeek()) m_incr = 600.0;
                break;
              case 0x5b42: // key down
              So just play around with that.

"""


class OMXPlayer(object):
    _FILEPROP_REXP = re.compile(r".*audio streams (\d+) video streams (\d+) chapters (\d+) subtitles (\d+).*")
    _VIDEOPROP_REXP = re.compile(r".*Video codec ([\w-]+) width (\d+) height (\d+) profile (\d+) fps ([\d.]+).*")
    _AUDIOPROP_REXP = re.compile(r"Audio codec (\w+) channels (\d+) samplerate (\d+) bitspersample (\d+).*")
    _STATUS_REXP = re.compile(r"V :\s*([\d.]+).*")
    _DONE_REXP = re.compile(r"have a nice day.*")

    _LAUNCH_CMD = '/usr/bin/omxplayer -s %s %s'       
    _PAUSE_CMD = 'p'
    _TOGGLE_SUB_CMD = 's'
    _QUIT_CMD = 'q'

	# nk additions: 
    _PREV_CH_CMD = 'i'
    _NEXT_CH_CMD = 'o'
    #_REW_CMD = '0x5b43'  #  left arrow
    _REW_CMD = '\x1B[D'  #  left arrow
    #_FF_CMD = '0x5b43' # right arrow
    _FF_CMD = '\x1B[C' # right arrow
    _VOL_UP_CMD = '+'
    _VOL_DOWN_CMD = '-'


	#######? 
	# "\x1B[A" for up
	# "\x1B[B" for down

    paused = False
    subtitles_visible = True

    #def __init__(self, mediafile, args=None, start_playback=False):
    def __init__(self, mediafile, args=None, start_playback=True):
        if not args:
            args = ""
        cmd = self._LAUNCH_CMD % (mediafile, args)
        self._process = pexpect.spawn(cmd)
        
        self.video = dict()
        self.audio = dict()
        # Get file properties
        #file_props = self._FILEPROP_REXP.match(self._process.readline()).groups()
        # (nk) breaking up above line into below two so I can see what's in that line in locals()... 
        _fp = self._process.readline()
		# FUCK!  So this is what's in -fp: 'Exception AttributeError:
		# AttributeError("\'_DummyThread\' object has no attribute \'_Thread__block\'",) in <module
		# \'threading\' from \'/usr/lib/python2.7/threading.pyc\'> ignored\r\n'  ................
		# WTF!!!!!!
		#   So.....   maybe now we're entering thread and process stuff I do not fucking understand.
		#   So, back to the old solution of hardcoding shit in and ignoring  shit
        #file_props = self._FILEPROP_REXP.match(_fp).groups()
        # NOPE, hardcoding instead of above:
        file_props = (1,1,0,0)
        (self.audio['streams'], self.video['streams'],
         self.chapters, self.subtitles) = [int(x) for x in file_props]

        """  
		 (nk)
	     Problem:     
		   (same issue as described here: http://www.raspberrypi.org/phpBB3/viewtopic.php?f=38&t=10599)
			video_props = self._VIDEOPROP_REXP.match(self._process.readline()).groups()
			AttributeError: 'NoneType' object has no attribute 'groups'
			(i.e. video_props is None) 
		 I think what's going on with the readlines is: 
			we're looking at the first lines that get printed after 'omxplayer blah.avi'. 
			The first line was expected to be the file props line; 
			the second line is the video line; 
			the third line is the audio props line.
		 However: 
			perhaps omxplayer has changed what it prints there since this was written; 
			here are the first four lines that get printed: 
				file : test.avi result 2 format avi audio streams 1 video streams 1 chapters 0 subtitles 0
				Aspect : num 1 den 1 aspect 2.352941 display aspect 1.500000
				Video codec omx-mpeg4 width 640 height 272 profile 15 fps 25.000000
				Audio codec mp3 channels 2 samplerate 48000 bitspersample 16
		 So - there's now that extra Aspect line -- so the line we want for video properties is the third one, not the ssecond one, and the line we want for audio properties is the fourth one, not the third one. 
         So I'm going to put an extra readline in here for the aspect properties but not do anything with it. 
			(as a quick fix -- later, check if the line begins with 'Aspect', 'Video', etc. etc., and if not, perhaps hardcode in the properties, e.g. just do self.audio = (blah, blah, blah, blah)  [ see https://github.com/jbaiter/pyomxplayer and what pprint(omx.__dict__) prints out in the readme for the order of things in that tuple]. )
        """
        _aspect_props = self._process.readline()
        print "\t=====> here we are after _aspect_props, here's locals:", locals()

        # Get video properties
        """ # not hardcoding video_props like with audio below... For now, at least, just not setting this shit is ok......
        video_props = self._VIDEOPROP_REXP.match(self._process.readline()).groups()
        self.video['decoder'] = video_props[0]
        self.video['dimensions'] = tuple(int(x) for x in video_props[1:3])
        self.video['profile'] = int(video_props[3])
        self.video['fps'] = float(video_props[4])
        """
        # Get audio properties
        #audio_props = self._AUDIOPROP_REXP.match(self._process.readline()).groups()
        audio_props = ('mp3',2,48000,16)    # nope, hardcoding instead of above 
        self.audio['decoder'] = audio_props[0]
        (self.audio['channels'], self.audio['rate'],
         self.audio['bps']) = [int(x) for x in audio_props[1:]]

        if self.audio['streams'] > 0:
            self.current_audio_stream = 1
            self.current_volume = 0.0

        self._position_thread = Thread(target=self._get_position)
        self._position_thread.start()
        print "\t=====> after self._position_thread.start()"



        if not start_playback:
            self.toggle_pause()
        self.toggle_subtitles()


    def _get_position(self):
        while True:
            index = self._process.expect([self._STATUS_REXP,
                                            pexpect.TIMEOUT,
                                            pexpect.EOF,
                                            self._DONE_REXP])
            if index == 1: continue
            elif index in (2, 3): break
            else:
                self.position = float(self._process.match.group(1))
            sleep(0.05)

    def toggle_pause(self):
        if self._process.send(self._PAUSE_CMD):
            self.paused = not self.paused

    def toggle_subtitles(self):
        if self._process.send(self._TOGGLE_SUB_CMD):
            self.subtitles_visible = not self.subtitles_visible
    def stop(self):
        self._process.send(self._QUIT_CMD)
        self._process.terminate(force=True)

    def set_speed(self):
        raise NotImplementedError

    def set_audiochannel(self, channel_idx):
        raise NotImplementedError

    def set_subtitles(self, sub_idx):
        raise NotImplementedError

    def set_chapter(self, chapter_idx):
        raise NotImplementedError

    def set_volume(self, volume):
        raise NotImplementedError

    def seek(self, minutes):
        raise NotImplementedError

	# all following nk additions
    def next_chapter(self):
		self._process.send(self._NEXT_CH_CMD)
    def prev_chapter(self):
		self._process.send(self._PREV_CH_CMD)
    def ff(self):
        self._process.send(self._FF_CMD)
    def rew(self):
		self._process.send(self._REW_CMD)
    def volume_up(self):
		self._process.send(self._VOL_UP_CMD)
    def volume_down(self):
		self._process.send(self._VOL_DOWN_CMD)
